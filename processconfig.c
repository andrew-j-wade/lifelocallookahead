#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include "arena.h"
#include "locallookahead.h"

void loadinitialrle(struct searchconfig *config, FILE *in,
                    char *buf, size_t *n)
{
    char rule[21];
    char *c;

    int xsize;
    int ysize;
    int arenax = config->arena.arenax;
    int margin = 2 * config->arena.period;
    int x;
    int y;
    int count;

    do {
        if (getline(&buf, n, in) == -1) {
            fprintf(stderr, "status file terminated inside rle pattern.\n");
            abort();
        };
        c = buf;
        while (isspace(*c)) {
            c++;
        };
    } while (*c == '#'); // find first non-comment line

     if (3 != sscanf(c, "x = %d , y = %d , rule = %20s",
                    &xsize, &ysize, rule)) {
        fprintf(stderr, "expecting rle header, got %s\n", buf);
        exit(EXIT_FAILURE);
    };

    config->arena.birthcounts = 0;
    config->arena.survivalcounts = 0;
    c = rule;
    while (*c != '\0' && *c != 'S') {
        if (*c >= '0' && *c <= '9') {
            config->arena.birthcounts |= 1 << (*c - '0');
        };
        c++;
    };
    if (c == '\0'){
        fprintf(stderr, "malformed rulestring, expecting B?/S?, got %s\n",
                rule);
        exit(EXIT_FAILURE);
    };
    while (*c != '\0') {
        if (*c >= '0' && *c <= '9') {
            config->arena.survivalcounts |= 1 << (*c - '0');
        };
        c++;
    };

    initarena(&config->arena);

    x=margin;
    y=margin;
    count = 0;
    while (getline(&buf, n, in) != -1) {
        c = buf;
        while (*c) {
            if (*c == 'b') {
                if (count == 0) {
                    count = 1;
                };
                x = x + count;
                count = 0;
            };
            if (*c == 'o') {
                if (count == 0) {
                    count = 1;
                };
                while (count > 0) {
                    assert(
                        x + margin < config->arena.arenax &&
                        y + margin < config->arena.arenay);

                    cellchange(&config->arena, x + y * arenax, C0N,
                               ROLLBACKNEVER);
                    cellchange(&config->arena, x + y * arenax, CN1,
                               ROLLBACKNEVER);
                    x++;
                    count--;
                };
            };
            if (*c == '$') {
                if (count == 0) {
                    count = 1;
                };
                y = y + count;
                x = margin;
                count = 0;
            };
            if (*c >= '0' && *c <= '9') {
                count = count * 10 + *c - '0';
            };
            if (*c == '!') {
                goto done;
            };
            if (*c == '#') {
                break; // skip comment line
            };
            c++;
        };
    };
done:
    ;
};

#define CLIPPOS(X) ((X) >= 1 ? (X) : 1)

void initializeoffsets(struct searchconfig *config)
{
    int margin = config->arena.period * 2;

    int arenax = config->arena.arenax;
    int i;

    struct searchnode *node;
    struct searchnode *othernode;

    int lookaheadcount;
    int cachecount;
    int lookaheadi;
    int cachei;
    int lastcachebit;

    lookaheadcount = 0;
    cachecount = 0;

    node = config->head;

    while (node) {
        node->offset = node->x[0] + margin + (node->y[0] + margin) * arenax;
        if (node->type == 'l') {
            lookaheadcount++;
        } else if (node->type == 'c') {
            cachecount++;
        } else {
            node->lookaheadcount = lookaheadcount;
            node->cachecount = cachecount;
            if (lookaheadcount > 0) {
                node->lookaheadoffsets = malloc(
                    sizeof(node->lookaheadoffsets[0]) * lookaheadcount);
                node->cachebits[0] = malloc(
                    sizeof(node->cachebits[0][0]) * lookaheadcount * 3);
                node->cachebits[1] = node->cachebits[0] + lookaheadcount;
                node->cachebits[2] = node->cachebits[1] + lookaheadcount;
            };
            if (cachecount > 0) {
                node->cacheoffsets = malloc(
                    sizeof(node->cacheoffsets[0]) * cachecount);

                node->cache3 = malloc(
                    (sizeof(uint64_t) << CLIPPOS(cachecount - 6)) +
                    (sizeof(uint64_t) << CLIPPOS(cachecount - 13)) +
                    (sizeof(uint64_t) << CLIPPOS(cachecount - 20)) +
                    (sizeof(uint64_t) << CLIPPOS(cachecount - 27)));

                node->cache2 = node->cache3 + (1 << CLIPPOS(cachecount - 27));
                node->cache1 = node->cache2 + (1 << CLIPPOS(cachecount - 20));
                node->cache0 = node->cache1 + (1 << CLIPPOS(cachecount - 13));
            };
            lookaheadi = 0;
            cachei = 0;
            while (lookaheadi < lookaheadcount || cachei < cachecount) {
                othernode = node->prev;
                if (othernode->type == 'l') {
                    node->prev = othernode->prev;
                    node->lookaheadoffsets[lookaheadi] = othernode->offset;
                    node->cachebits[0][lookaheadi] = -1;
                    node->cachebits[1][lookaheadi] = -1;
                    node->cachebits[2][lookaheadi] = -1;
                    lookaheadi++;
                    free(othernode);
                } else if (othernode->type == 'c') {
                    node->prev = othernode->prev;
                    node->cacheoffsets[cachei] = othernode->offset;
                    cachei++;
                    free(othernode);
                } else {
                    fprintf(stderr, "Internal error in initializeoffsets\n");
                    fprintf(stderr, "For node at %d, %d\n",
                            node->x[0], node->y[0]);
                    abort();
                };
            };
            if (node->prev) {
                node->prev->next = node;
            } else {
                config->head = node;
            };
            lookaheadcount = 0;
            cachecount = 0;
        };
        node = node->next;
    };

    /* check consistency and assign cachenodes */

    node = config->head;

    while(node) {
        othernode = node->prev;
        while (othernode) {
            if (othernode->offset == node->offset) {
                fprintf(stderr, "Duplicate node at %d, %d\n",
                        node->x[0], node->y[0]);
                exit(EXIT_FAILURE);
            };
            othernode = othernode->prev;
        };

        for (i = 0; i < 2; i++) {
            if (node->x[i+1] >= 0) {
                othernode = node->next;
                while (othernode) {
                    if (othernode->x[0] == node->x[i+1] &&
                        othernode->y[0] == node->y[i+1]) {
                        break;
                     };
                    othernode = othernode->next;
                };
                if (othernode && othernode->cachecount > 0) {
                    node->cachenodes[i] = othernode;
                } else {
                    fprintf(stderr, "Invalid cache node\n");
                    fprintf(stderr, "For node at %d, %d\n",
                            node->x[0], node->y[0]);
                    exit(EXIT_FAILURE);
                };
            } else {
                node->cachenodes[i] = NULL;
            };
        };

        node = node->next;
    };

    /* find thresholds and check consistency of cache */

    node = config->head;

    while(node) {
        for (lookaheadi = 0; lookaheadi < node->lookaheadcount; lookaheadi++) {
            for (i = 0; i < lookaheadi; i++) {
                if (node->lookaheadoffsets[i] ==
                    node->lookaheadoffsets[lookaheadi]) {
                    fprintf(stderr, "Duplicate lookahead node\n");
                    fprintf(stderr, "For node at %d, %d\n",
                            node->x[0], node->y[0]);
                    exit(EXIT_FAILURE);
                };
            };

            othernode = node->prev;

            while (othernode) {
                if (othernode->offset ==
                    node->lookaheadoffsets[lookaheadi]) {
                    break;
                };
                othernode = othernode->prev;
            };
            if (!othernode) {
                fprintf(stderr, "Lookahead outside arena\n");
                fprintf(stderr, "For node at %d, %d\n",
                        node->x[0], node->y[0]);
                exit(EXIT_FAILURE);
            };
        };

        node->threshold = 0;
        for (cachei = 0; cachei < node->cachecount; cachei++) {
            lookaheadi = 0;
            while (lookaheadi < node->lookaheadcount) {
                if (node->cacheoffsets[cachei] ==
                    node->lookaheadoffsets[lookaheadi]) {
                    break;
                };
                lookaheadi++;
            };
            if (lookaheadi < node->lookaheadcount) {
                if (lookaheadi >= node->threshold) {
                    node->threshold = lookaheadi + 1;
                };
            } else {
                fprintf(stderr, "Cache node not in lookaheads\n");
                fprintf(stderr, "For node at %d, %d\n",
                        node->x[0], node->y[0]);
                exit(EXIT_FAILURE);
            };
            node->cachebits[0][lookaheadi] = node->cachecount - cachei - 1;
        };

        for (i = 0; i < 2; i++) {
            if (!node->cachenodes[i]) {
                continue;
            };

            for (cachei = 0;
                 cachei < node->cachenodes[i]->cachecount;
                 cachei++) {
                othernode = node->cachenodes[i]->prev;
                while (othernode != node->prev) {
                    if (othernode->offset ==
                        node->cachenodes[i]->cacheoffsets[cachei]) {
                        break;
                    }
                    othernode = othernode->prev;
                };

                if (othernode == node->prev) {
                    lookaheadi = 0;
                    while (lookaheadi < node->lookaheadcount) {
                        if (node->lookaheadoffsets[lookaheadi] ==
                            node->cachenodes[i]->cacheoffsets[cachei]) {
                            break;
                        };
                        lookaheadi++;
                    };

                    if (lookaheadi < node->lookaheadcount) {
                        if (lookaheadi >= node->threshold) {
                            node->threshold = lookaheadi + 1;
                        };
                    } else {
                        fprintf(stderr, "Invalid node for cache lookup\n");
                        fprintf(stderr, "For node at %d, %d\n",
                                node->x[0], node->y[0]);
                        exit(EXIT_FAILURE);
                    };
                    node->cachebits[i+1][lookaheadi] =
                        node->cachenodes[i]->cachecount - cachei - 1;
                };
            };
        };

        /* Check that the cache bits are in order. */
        for (i = 0; i < 3; i++) {
            lastcachebit = 64;
            for (lookaheadi = 0;
                 lookaheadi < node->lookaheadcount;
                 lookaheadi++) {
                if (node->cachebits[i][lookaheadi] > lastcachebit) {
                    fprintf(stderr, "Cache nodes out of order\n");
                    fprintf(stderr, "For node at %d, %d\n",
                            node->x[0], node->y[0]);
                    exit(EXIT_FAILURE);
                };

                if (node->cachebits[i][lookaheadi] >= 0) {
                    lastcachebit = node->cachebits[i][lookaheadi];
                };
            };
        };

        node = node->next;
    };

    /* Node type 'A' is initalized to NULL in the arena and
     * doesn't form part of the seach tree.
     */

    node = config->head;

    while (node && node->type == 'A') {
        cellchange(&config->arena, node->offset, CxN, ROLLBACKNEVER);
        config->head = node->next;
        free(node);
        node = config->head;
    };

    if (node) {
        node->prev = NULL;
    };

};



void processconfig(struct searchconfig *config, FILE *in, FILE *out)
{
    char *buf = NULL;
    char *l;
    size_t n = 0;
    struct searchnode *node;
    int x[3], y[3];
    int i;
    long sec, usec;
    char nodetype;

    char rule[21];

    float decayrate;
    float mutatepenalty;

    config->head = NULL;
    config->tail = NULL;
    config->ru_utime.tv_sec = 0;
    config->ru_utime.tv_usec = 0;
    config->successcount = 0;
    config->failurecount = 0;

    config->arena.queue = NULL;
    config->arena.cells = NULL;

    while (getline(&buf, &n, in) != -1) {
        l = buf;
        while (isspace(*l)) {
            l++;
        };


        if (sscanf(l, "startrle %ld.%ld %ld %ld", &sec, &usec,
            &config->successcount, &config->failurecount) >= 2) {

            config->ru_utime.tv_sec = sec;
            config->ru_utime.tv_usec = usec;

            config->arena.arenax = 0;
            config->arena.arenay = 0;
            for (node = config->head; node; node = node->next) {
                if (node->x[0] + 4 * config->arena.period + 1>
                    config->arena.arenax) {
                    config->arena.arenax =
                        node->x[0] + 4 * config->arena.period + 1;
                };
                if (node->y[0] + 4 * config->arena.period + 1 >
                    config->arena.arenay) {
                    config->arena.arenay =
                        node->y[0] + 4 * config->arena.period + 1;
                };
            };
            cleanuparena(&config->arena);
            loadinitialrle(config, in, buf, &n);
            continue;
        } else if (0 == strncmp(l, "stats", 5)) {
            continue;
        };

        /* Most of the config file we print out exactly as we receive it,
         * thus preserving formatting, comments, etc.
         * The exceptions are startrle and stats (above) as they will be
         * updated.
         */
        fprintf(out, "%s", buf);

        if (l[0] == '\0' || l[0] == '#') {
            continue;
        };

        if (3 == sscanf(l, "glider ( %d , %d ) c / %d",
                        &config->arena.x,
                        &config->arena.y,
                        &config->arena.period)) {
            continue;
        };

        for (i = 0; i < 3; i++) {
            x[i] = -1;
            y[i] = -1;
        };

        if (3 <= sscanf(l, " %c %d , %d , %d , %d , %d , %d",
                        &nodetype, &x[0], &y[0],
                        &x[1], &y[1], &x[2], &y[2]) &&
            strchr("nlmNAc", nodetype)) {
            node = malloc(sizeof(*node));
            assert(node);
            for (i = 0; i < 3; i++) {
                node->x[i] = x[i];
                node->y[i] = y[i];
            };
            node->type = nodetype;
            node->cachepopulated = 0;
            node->prev = config->tail;
            if (node->prev) {
                node->prev->next = node;
            };
            node->next = NULL;
            config->tail = node;
            if (!config->head) {
                config->head = node;
            };
        };

    };
    free(buf);

    initializeoffsets(config);
};
