
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "arena.h"

const unsigned char changetable[3][3] =
{
    {0, 0, C0N},
    {0, 0, C1N},
    {CN0, CN1, 0}
};

void initarenalookup(struct arena *arena)
{
    int i;
    int activecells;
    int nullcells;
    int j;
    int cellset;
    int cellclear;
    uint16_t *table = &arena->transitiontable[0];
    unsigned char *statestatus = &arena->statestatus[0];


    i = 0;
    for (activecells = 0; activecells < 9; activecells++) {
        for (nullcells = 0; nullcells < 9 - activecells; nullcells++) {
            cellset = 0;
            cellclear = 0;
            for (j = 0; j <= nullcells; j++) {
                if ((arena->birthcounts & (1 << (activecells + j))) != 0) {
                    cellset = 1;
                } else {
                    cellclear = 1;
                };
            };
            if (cellset && cellclear) {
                statestatus[i] = CELLNULL;
            } else if (cellset) {
                statestatus[i] = CELLSET;
            } else {
                statestatus[i] = CELLCLEAR;
            };

            cellset = 0;
            cellclear = 0;
            for (j = 0; j <= nullcells; j++) {
                if ((arena->survivalcounts & (1 << (activecells + j))) != 0) {
                    cellset = 1;
                } else {
                    cellclear = 1;
                };
            };
            if (cellset && cellclear) {
                statestatus[45+i] = CELLNULL;
            } else if (cellset) {
                statestatus[45+i] = CELLSET;
            } else {
                statestatus[45+i] = CELLCLEAR;
            };

            if (statestatus[i] == statestatus[45+i]) {
                statestatus[90+i] = statestatus[i];
            } else {
                statestatus[90+i] = CELLNULL;
            };

            i++;
        };
    };

    for (i = 0; i < 45; i++) {
        table[90+i] =
            (changetable[statestatus[90+i]][statestatus[i]] << 8) + i;
        table[0x100+90+i] =
            (changetable[statestatus[90+i]][statestatus[45+i]] << 8) + 45 + i;
        table[0x200+45+i] =
            (changetable[statestatus[45+i]][statestatus[90+i]] << 8) + 90 + i;
        table[0x300+i] =
            (changetable[statestatus[i]][statestatus[90+i]] << 8) + 90 + i;
    };


    i = 0;
    for (activecells = 0; activecells < 9; activecells++) {
        for (nullcells = 0; nullcells < 9 - activecells; nullcells++) {
            for (j=0; j <= 90; j += 45) {
                if (nullcells > 0) {
                    table[0x400+j+i] =
                        (changetable[statestatus[j+i]]
                          [statestatus[j+i-1]] << 8) +
                        j + i - 1;;
                    table[0x500+j+i] =
                        (changetable[statestatus[j+i]]
                          [statestatus[j + i + (8 - activecells)]] << 8) +
                        j + i + (8 - activecells);
                };
                if (activecells > 0) {
                    table[0x600+j+i] =
                        (changetable[statestatus[j+i]]
                          [statestatus[j + i - (9 - activecells)]] << 8) +
                        j + i - (9 - activecells);
                };
                if (activecells + nullcells < 8) {
                    table[0x700+j+i] =
                        (changetable[statestatus[j+i]]
                          [statestatus[j+i+1]] << 8) +
                        j + i + 1;
                };
            };
            i = i + 1;
        };
    };


};


void initarena(struct arena *arena)
{
    int arenax = arena->arenax;
    int arenay = arena->arenay;

    int genoffset = arenax * arenay;

    int p = arena->period + 1;


    initarenalookup(arena);

    arena->queue = malloc(sizeof(struct changeentry) *
                          ((4 * p * p * p - p ) / 3));

    arena->cells = calloc(arenax * arenay * (arena->period + 1),1);

    assert(arena->queue && arena->cells);

    arena->genoffset = genoffset;

    arena->recurrenceoffset = - genoffset * arena->period -
        arena->y * arenax - arena->x;

    arena->discrepancycount = 0;
};

void cleanuparena(struct arena *arena)
{
    free(arena->queue);
    free(arena->cells);
};

struct initialstatetransition {
    uint16_t transition;
    signed char discrepancychange;
};

struct recstatetransition {
    unsigned char nextstate;
    signed char discrepancychange;
};

const struct initialstatetransition setstatetable[4][16] =
{
 /* CN0 */
 {
  {0, 0},               /* 0000 */
  {0, 0},               /* 0001 */
  {CN0 << 8, 0},        /* 0010 */
  {CN0 << 8, 0},        /* 0011 */
  {4, 0},               /* 0100 */
  {4, 0},               /* 0101 */
  {(CN0 << 8) + 4, 1},  /* 0110 */
  {(CN0 << 8) + 4, 1},  /* 0111 */
  {8, 0},               /* 1000 */
  {8, 0},               /* 1001 */
  {(CN0 << 8) + 8, 0},  /* 1010 */
  {(CN0 << 8) + 8, 0},  /* 1011 */
  {8, 0},               /* 1100 */
  {8, 0},               /* 1101 */
  {(CN0 << 8) + 8, 0},  /* 1110 */
  {(CN0 << 8) + 8, 0}   /* 1111 */
 },
 /* CN1 */
 {
  {1, 0},               /* 0000 */
  {1, 0},               /* 0001 */
  {(CN1 << 8) + 1, 1},  /* 0010 */
  {(CN1 << 8) + 1, 1},  /* 0011 */
  {5, 0},               /* 0100 */
  {5, 0},               /* 0101 */
  {(CN1 << 8) + 5, 0},  /* 0110 */
  {(CN1 << 8) + 5, 0},  /* 0111 */
  {9, 0},               /* 1000 */
  {9, 0},               /* 1001 */
  {(CN1 << 8) + 9, 0},  /* 1010 */
  {(CN1 << 8) + 9, 0},  /* 1011 */
  {9, 0},               /* 1100 */
  {9, 0},               /* 1101 */
  {(CN1 << 8) + 9, 0},  /* 1110 */
  {(CN1 << 8) + 9, 0}   /* 1111 */
 },
 /* CxN */
 {
  {(C0N << 8) + 2, 0},  /* 0000 */
  {(C1N << 8) + 2, -1}, /* 0001 */
  {2, 0},               /* 0010 */
  {2, 0},               /* 0011 */
  {(C0N << 8) + 6, -1}, /* 0100 */
  {(C1N << 8) + 6, 0},  /* 0101 */
  {6, 0},               /* 0110 */
  {6, 0},               /* 0111 */
  {(C0N << 8) + 10, 0}, /* 1000 */
  {(C1N << 8) + 10, 0}, /* 1001 */
  {10, 0},              /* 1010 */
  {10, 0},              /* 1011 */
  {(C0N << 8) + 10, 0}, /* 1100 */
  {(C1N << 8) + 10, 0}, /* 1101 */
  {10, 0},              /* 1110 */
  {10, 0}               /* 1111 */
 },
 /* C1N same as CxN */
 {
  {(C0N << 8) + 2, 0},  /* 0000 */
  {(C1N << 8) + 2, -1}, /* 0001 */
  {2, 0},               /* 0010 */
  {2, 0},               /* 0011 */
  {(C0N << 8) + 6, -1}, /* 0100 */
  {(C1N << 8) + 6, 0},  /* 0101 */
  {6, 0},               /* 0110 */
  {6, 0},               /* 0111 */
  {(C0N << 8) + 10, 0}, /* 1000 */
  {(C1N << 8) + 10, 0}, /* 1001 */
  {10, 0},              /* 1010 */
  {10, 0},              /* 1011 */
  {(C0N << 8) + 10, 0}, /* 1100 */
  {(C1N << 8) + 10, 0}, /* 1101 */
  {10, 0},              /* 1110 */
  {10, 0}               /* 1111 */
 }
};

const struct recstatetransition recstatetable[4][16] =
{
    /* CN0 */
    {
        {0, 0},     /* 0000 */
        {0, 0},     /* 0001 */
        {0, 0},     /* 0010 */
        {0, 0},     /* 0011 */
        {0, 0},     /* 0100 */
        {0, 0},     /* 0101 */
        {0, 0},     /* 0110 */
        {0, 0},     /* 0111 */
        {0, 0},     /* 1000 */
        {1, 1},     /* 1001 */
        {2, 0},     /* 1010 */
        {2, 0},     /* 1011 */
        {0, 0},     /* 1100 */
        {1, 1},     /* 1101 */
        {2, 0},     /* 1110 */
        {2, 0}      /* 1111 */
    },
    /* CN1 */
    {
        {0, 0},     /* 0000 */
        {0, 0},     /* 0001 */
        {0, 0},     /* 0010 */
        {0, 0},     /* 0011 */
        {0, 0},     /* 0100 */
        {0, 0},     /* 0101 */
        {0, 0},     /* 0110 */
        {0, 0},     /* 0111 */
        {4, 1},     /* 1000 */
        {5, 0},     /* 1001 */
        {6, 0},     /* 1010 */
        {6, 0},     /* 1011 */
        {4, 1},     /* 1100 */
        {5, 0},     /* 1101 */
        {6, 0},     /* 1110 */
        {6, 0}      /* 1111 */
    },
    /* C1N */
    {
        {0, 0},     /* 0000 */
        {0, 0},     /* 0001 */
        {0, 0},     /* 0010 */
        {0, 0},     /* 0011 */
        {8, -1},    /* 0100 */
        {9, 0},     /* 0101 */
        {10, 0},    /* 0110 */
        {10, 0},    /* 0111 */
        {0, 0},     /* 1000 */
        {0, 0},     /* 1001 */
        {0, 0},     /* 1010 */
        {0, 0},     /* 1011 */
        {0, 0},     /* 1100 */
        {0, 0},     /* 1101 */
        {0, 0},     /* 1110 */
        {0, 0}      /* 1111 */
    },
    /* C0N */
    {
        {8, 0},     /* 0000 */
        {9, -1},    /* 0001 */
        {10, 0},    /* 0010 */
        {10, 0},    /* 0011 */
        {0, 0},     /* 0100 */
        {0, 0},     /* 0101 */
        {0, 0},     /* 0110 */
        {0, 0},     /* 0111 */
        {0, 0},     /* 1000 */
        {0, 0},     /* 1001 */
        {0, 0},     /* 1010 */
        {0, 0},     /* 1011 */
        {0, 0},     /* 1100 */
        {0, 0},     /* 1101 */
        {0, 0},     /* 1110 */
        {0, 0}      /* 1111 */
    }
};



int cellchange(struct arena *arena, int offset, int change,
               enum rollbackbehavior rollback)
{
    int discrepancycount = arena->discrepancycount;

    unsigned char* cell = &arena->cells[offset];

    int genleft = arena->period;
    int arenax = arena->arenax;

    int genoffset = arena->genoffset;

    int recurrenceoffset;

    uint16_t transition;

    const struct initialstatetransition *initialtransition;
    const struct recstatetransition *rectransition;
    const uint16_t *statetable;

    struct changeentry *queue = arena->queue;
    struct changeentry *queuenextgen;
    struct changeentry *queueend = queue + 1;

    initialtransition = &setstatetable[change-1][*cell];
    transition = initialtransition->transition;
    assert(transition > 0xFF);

    discrepancycount += initialtransition->discrepancychange;

    *cell = transition & 0xFF;

    queue->cell = cell;
    queue->transition = transition;

    while (genleft-- > 0) {
        queuenextgen = queueend;
        while (queue < queuenextgen) {
            statetable = &arena->transitiontable
                [(queue->transition & 0xFF00) - 256];

            /* The writes to queueend below are unconditional but aren't
             * effective until queueend is incremented.
             * By making the writes unconditional we allow the optimizer
             * to avoid branch mispredictions by using a cmov for
             * queueend++.
             */

            cell = queue->cell + genoffset;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            statetable += 4 * 256;

            cell -= (arenax + 1);
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += arenax;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell -= 2;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += arenax;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;
            queueend->cell = cell;
            queueend->transition = transition;
            if (transition > 255) { queueend++; };

            queue++;
        };

    };

    recurrenceoffset = arena->recurrenceoffset;

    while (queue < queueend) {
        cell = queue->cell + recurrenceoffset;
        transition = queue->transition;
        rectransition = &recstatetable[(transition >> 8) - 1][*cell];
        *cell = rectransition->nextstate;
        discrepancycount += rectransition->discrepancychange;
        queue++;
    };

    if (rollback != ROLLBACKNEVER && (rollback == ROLLBACKALWAYS ||
        discrepancycount > 0)) {
        cell = &arena->cells[offset];
        initialtransition = &setstatetable[4-change][*cell];
        transition = initialtransition->transition;
        *cell = transition & 0xFF;

        queue = arena->queue;
        while (queue < queuenextgen) {
            statetable = &arena->transitiontable
                [0x0400 - (queue->transition & 0xFF00)];

            cell = queue->cell + genoffset;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            statetable += 4 * 256;

            cell -= (arenax + 1);
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += arenax;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell -= 2;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += arenax;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            cell += 1;
            transition = statetable[*cell];
            *cell = transition & 0xFF;

            queue++;
        };

        while (queue < queueend) {
            cell = queue->cell + recurrenceoffset;
            transition = queue->transition;
            rectransition = &recstatetable[4 - (transition >> 8)][*cell];
            *cell = rectransition->nextstate;
            queue++;
        };
    } else {
        arena->discrepancycount = discrepancycount;
    };

    return discrepancycount == 0;
};