
#include <stdio.h>
#include "arena.h"

#define ARENASIZE 100
 
void dumparena(FILE * f, struct arena * arena,
               int ystart, int yend)
{
    int g;
    int y;
    int x;

    for (y = ystart; y < yend; y++) {
        for (x = 0; x < 79; x++ ) {
            if (arena->cells[y * ARENASIZE + x] & NULLMASK) {
                fputc('N', f);
            } else if (arena->cells[y*ARENASIZE + x] & CELLMASK) {
                fputc('1', f);
            } else {
                fputc(' ', f);
            };
        };
        fputc('\n', f);
    };

    for (g = 1; g <= arena->period; g++) {
        fprintf(f, "%d:\n", g);
        for (y = ystart; y < yend; y++) {
            for (x = 0 ; x < 79; x++ ) {
                fputc(" 1N"[arena->statestatus[arena->cells[
                    g * ARENASIZE * ARENASIZE + y * ARENASIZE + x]]],
                    f);
            };
            fputc('\n', f);
        };

        for (y = ystart; y < yend; y++) {
            for (x = 0; x < 26; x++) {
                fprintf(f, "%2x%c",
                    arena->cells[
                    g * ARENASIZE * ARENASIZE + y * ARENASIZE + x],
                    " 1N"[arena->statestatus[arena->cells[
                    g * ARENASIZE * ARENASIZE + y * ARENASIZE + x]]]
                       );
            };
            fputc('\n', f);
        };
    };
};

int main (int argc, char *argv[])
{
    struct arena arena;
    int i;

    arena.x = 2;
    arena.y = 0;
    arena.arenax = ARENASIZE;
    arena.arenay = ARENASIZE;
    arena.period = 4;
    arena.birthcounts = 1<<3;
    arena.survivalcounts = (1<<2) + (1<<3);

    initarena(&arena);

    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 10, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 11, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 12, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 13, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 14, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 15, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 16, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 16, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  9 * ARENASIZE + 16, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  8 * ARENASIZE + 15, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  7 * ARENASIZE + 13, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  7 * ARENASIZE + 12, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  8 * ARENASIZE + 10, CxN, ROLLBACKNEVER));

    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 10, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 11, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 12, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 13, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 14, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 15, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 11 * ARENASIZE + 16, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 16, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  9 * ARENASIZE + 16, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  8 * ARENASIZE + 15, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  7 * ARENASIZE + 13, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  7 * ARENASIZE + 12, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena,  8 * ARENASIZE + 10, CN1, ROLLBACKNEVER));

    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 12, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 13, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 12, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 13, CN1, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 12, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 13, CxN, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 12, CN0, ROLLBACKNEVER));
    printf("%d\n", cellchange(&arena, 10 * ARENASIZE + 13, CN0, ROLLBACKNEVER));

    dumparena(stdout, &arena, 0, 20);

};