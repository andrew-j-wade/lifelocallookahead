#!/usr/bin/python3

import itertools

import random

import collections

glider = "(2,1)c/6"

class Node:
    __slots__ = ["lookahead", "cache", "cache1search", "cache2search"]

    def __init__(self):
        self.lookahead = []
        self.cache = []
        self.cache1search = []
        self.cache2search = []


def generatediagonal():

    w = [10, 10, 11, 11, 11,
         12, 12, 13, 13, 13,
         14, 14, 15, 15, 15,
         16, 16, 17, 17, 17,
         18, 18, 19, 19, 19,
         20, 20, 20, 20, 21, 21, 21, 21, 21,
         22, 22, 22, 22, 22, 22, 22, 22, 22]

    cacherows = 5
    sortpolarity = +1

    o = w[0] // 2

    csize = {10: 2,
             11: 2,
             12: 3,
             13: 3,
             14: 4,
             15: 4,
             16: 5,
             17: 5,
             18: 6,
             19: 6,
             20: 7,
             21: 7,
             22: 7,
             23: 7,
             24: 7}

    doublerows = [([(x + y, o + x - y) for y in range(w[x] // 2)],
                   [(x + y, o + 1 + x - y) for y in
                                           reversed(range((w[x] + 1) // 2))])
                  for x in range(len(w))]

    nodes = {}

    lastnode = None

    for dr in doublerows:
        for r in dr:
            for pos in r:
                nodes[pos] = Node()
                if lastnode:
                    nodes[lastnode].cache1search = [pos]
                lastnode = pos

    # adjust the row ends to wrap around into the next row
    # also set cache2search references
    for x in range(len(w)):
        if csize[w[x]] < 2:
            adjust = 0
        elif csize[w[x]] < 4:
            adjust = 1
        elif csize[w[x]] < 6:
            adjust = 2
        else:
            adjust = 3


        if adjust:
            doublerows[x][1][:0] = doublerows[x][0][-adjust:]
            del doublerows[x][0][-adjust:]

        for pos in doublerows[x][0]:
            nodes[pos].cache2search = doublerows[x][1]

        if x < len(w) - 1:
            if adjust:
                doublerows[x+1][0][:0] = doublerows[x][1][-adjust:]
                del doublerows[x][1][-adjust:]

            for pos in doublerows[x][1]:
                nodes[pos].cache2search = doublerows[x+1][0]

    prevnodes = set()

    for x in range(len(w)):
        lookaheadsize = csize[w[x]] + 2
        dx = len(doublerows[x][0]) - 1
        # The first few entries might have been added from the
        # previous row, so generate startpos from the last entry
        startpos = (doublerows[x][0][-1][0] - dx,
                    doublerows[x][0][-1][1] + dx)
        for dx in range(len(doublerows[x][0])):
            pos = (startpos[0] + dx,
                   startpos[1] - dx)

            lookahead = [(pos[0] + lx, pos[1] - ly)
                         for ly in range(lookaheadsize)
                         for lx in range(lookaheadsize)
                         if (pos[0] + lx, pos[1] - ly) in prevnodes
                         and not (lx == 0 and ly + 1 == lookaheadsize)]

            cache = []

            for cr in range(cacherows):
                for lx in range(csize[w[x]] - cr):
                    cache.append((pos[0] + lx, pos[1] - lx - 1 - cr))

            cache = [c for c in cache if c in prevnodes]

            nodes[doublerows[x][0][dx]].lookahead = lookahead
            nodes[doublerows[x][0][dx]].cache = cache

            prevnodes.add(doublerows[x][0][dx])

        dx = len(doublerows[x][1]) - 1
        # The first few entries might have been added from the
        # previous row, so generate startpos from the last entry
        startpos = (doublerows[x][1][-1][0] + dx,
                    doublerows[x][1][-1][1] - dx)
        for dx in range(len(doublerows[x][1])):
            # We might have extra entries added to the beginning of the
            # row, so use the last entry to regenerate all the row positions

            pos = (startpos[0] - dx,
                   startpos[1] + dx)

            lookahead = [(pos[0] - lx, pos[1] + ly)
                         for ly in range(lookaheadsize)
                         for lx in range(lookaheadsize)
                         if (pos[0] - lx, pos[1] + ly) in prevnodes
                         and not (lx + 1 == lookaheadsize and ly == 0)]

            cache = []

            for cr in range(cacherows):
                for lx in range(csize[w[x]] - cr):
                    cache.append((pos[0] - lx - 1 - cr, pos[1] + lx))

            cache = [c for c in cache if c in prevnodes]

            nodes[doublerows[x][1][dx]].lookahead = lookahead
            nodes[doublerows[x][1][dx]].cache = cache

            prevnodes.add(doublerows[x][1][dx])

    #second pass: find links and adjust lookahead as needed

    prevnodes = set()

    def printnode(pos, nodes, prevnodes):

        def sortkey(c):
            return (c[0] + c[1],
                    sortpolarity * (((c[0] + c[1]) % 2) * 2 - 1) * c[0])

        lookahead = nodes[pos].lookahead
        cache = nodes[pos].cache
        cache1search = nodes[pos].cache1search
        cache2search = nodes[pos].cache2search

        if len(cache) == 0:
            print("n {}, {}".format(*pos))
        else:
            cache1 = []
            for cache1pos in reversed(cache1search):
                cache1 = [c for c in nodes[cache1pos].cache
                          if c in prevnodes]
                if (len(cache1) > 0 and
                    len([c for c in cache1 if not c in lookahead]) == 0):
                    break

            cache1 = [c for c in cache1 if c in prevnodes]

            cache2 = []
            for cache2pos in reversed(cache2search):
                cache2 = [c for c in nodes[cache2pos].cache
                          if c in prevnodes]
                if (len(cache2) > 0 and
                    len([c for c in cache2 if not c in lookahead]) == 0):
                    break

            cache2 = [c for c in cache2 if c in prevnodes]

            # add any needed nodes to lookahead
            lookahead = sorted(set(lookahead) | set(cache1) | set(cache2),
                               key = sortkey)
            cache = sorted(cache, key = sortkey)

            anycache = set(cache) | set(cache1) | set(cache2)

            if len([c for c in cache2 if not c in cache1]) == 0:
                cache2 = [] # subset of cache1

            if len([c for c in cache1 if not c in cache2]) == 0:
                cache1 = [] # subset of cache2

            for l in lookahead:
                if l not in anycache:
                    print("l {}, {}".format(*l))
            for l in lookahead:
                if l in anycache:
                    print("l {}, {}".format(*l))
            for c in cache:
                print("c {}, {}".format(*c))
            if len(cache1) == 0:
                if len(cache2) == 0:
                    print("n {}, {}".format(*pos))
                else:
                    print("n {}, {}, {}, {}".format(pos[0],
                                                    pos[1],
                                                    cache2pos[0],
                                                    cache2pos[1]))
            else:
                if len(cache2) == 0:
                    print("n {}, {}, {}, {}".format(pos[0],
                                                    pos[1],
                                                    cache1pos[0],
                                                    cache1pos[1]))
                else:
                    print("n {}, {}, {}, {}, {}, {}".format(pos[0],
                                                            pos[1],
                                                            cache1pos[0],
                                                            cache1pos[1],
                                                            cache2pos[0],
                                                            cache2pos[1]))

    for x in range(len(w)):

        if x == len(w) - 1:
            startingnodes = set(random.sample(prevnodes, len(prevnodes) // 2))

        for r in doublerows[x]:
            for pos in r:

                printnode(pos, nodes, prevnodes)

                prevnodes.add(pos)


    return startingnodes

def printrle(nodes):

    maxx = max(x for (x, y) in nodes)
    maxy = max(y for (x, y) in nodes)

    print("x = {}, y = {}, rule = B3/S23".format(maxx + 1, maxy + 1))

    for y in range(maxy+1):
        print("".join("o" if (x, y) in nodes else "b"
                      for x in range(maxx+1)) + "$")
    print("!")


if __name__ == "__main__":
    print("glider {}".format(glider))

    startingnodes = generatediagonal()
    print("#stats  cputime  lookahead positive  lookahead negative")
    print("startrle 0.000000 0 0")

    printrle(startingnodes)

