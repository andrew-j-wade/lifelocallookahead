
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "arena.h"
#include "locallookahead.h"

sig_atomic_t volatile terminate = 0;

void
sigtermhandler(int signum)
{
    terminate = signum;
};

void print_ru_utime(const char *kword,
                    struct searchconfig *config,
                    FILE *f)
{
    struct rusage rusage;
    long sec, usec;

    rusage.ru_utime.tv_sec = 0;
    rusage.ru_utime.tv_usec = 0;

    if (0 == getrusage(RUSAGE_SELF, &rusage)) {
        sec = config->ru_utime.tv_sec + rusage.ru_utime.tv_sec;
        usec = config->ru_utime.tv_usec + rusage.ru_utime.tv_usec;
        if (usec >= 1000000) {
            sec++;
            usec -= 1000000;
        };
    } else {
        fprintf(stderr, "Error calling getrusage()\n");
        abort();
    };
    fprintf(f, "%s %ld.%06ld %ld %ld\n", kword, sec, usec,
            config->successcount + successcount,
            config->failurecount + failurecount);
};

void print_stats(const char *kword,
                 FILE *f)
{
    struct rusage rusage;

    if (0 != getrusage(RUSAGE_SELF, &rusage)) {
        fprintf(stderr, "Error calling getrusage()\n");
        abort();
    };

    fprintf(f, "%s %ld.%06ld %ld %ld\n", kword,
            (long) rusage.ru_utime.tv_sec, (long) rusage.ru_utime.tv_usec,
            successcount,
            failurecount);
};


void printrle(struct arena *arena, FILE *f)
{
    int arenax = arena->arenax;
    int arenay = arena->arenay;
    int margin = 2 * arena->period;
    unsigned char *cells = arena->cells;

    int x;
    int y;
    int i;
    int currentrunlength;
    char currentstate;
    char nextstate;
    int dollarcount;
    int currentlinelength;

    fprintf(f, "x = %d, y = %d, rule = B", arenax - 2 * margin,
                                           arenay - 2 * margin);
    for (i = 0; i <= 8; i++) {
        if (arena->birthcounts & (1 << i)) {
            fprintf(f, "%d", i);
        };
    };
    fputs("/S", f);
    for (i = 0; i <= 8; i++) {
        if (arena->survivalcounts & (1 << i)) {
            fprintf(f, "%d", i);
        };
    };

    fputc('\n', f);

    dollarcount = 0;
    currentlinelength = 0;

    for (y = margin; y < arenay; y++) {
        currentrunlength = 0;
        currentstate = 'b';
        for (x = margin; x < arenax; x++) {
            if (cells[x + y * arenax] & CELLMASK) {
                nextstate = 'o';
            } else {
                nextstate = 'b';
            };
            if (nextstate == currentstate) {
                currentrunlength++;
            } else {
                if (currentlinelength > 65) {
                    fputc('\n', f);
                    currentlinelength = 0;
                };
                if (dollarcount > 0) {
                    if (dollarcount == 1) {
                        fputc('$', f);
                        currentlinelength++;
                    } else {
                        currentlinelength +=
                            fprintf(f, "%d$", dollarcount);
                    };
                    dollarcount = 0;
                };
                if (currentlinelength > 60) {
                    fputc('\n', f);
                    currentlinelength = 0;
                };
                if (currentrunlength > 1) {
                    currentlinelength +=
                        fprintf(f, "%d%c", currentrunlength, currentstate);
                } else if (currentrunlength == 1) {
                    fputc(currentstate, f);
                    currentlinelength++;
                };
                currentrunlength = 1;
                currentstate = nextstate;
            };
        };
        dollarcount++;
    };
    fprintf(f, "!\n");
};

const char *dbglook(struct searchnode *node, struct arena *arena)
{
    static char buffer[80];
    int i;
    int offset = 0;

    for (i = 0; i < node->lookaheadcount && i < 60; i++) {
        if (node->lookaheadoffsets[i] == offset) {
            buffer[i] = '#';
        } else {
            offset = node->lookaheadoffsets[i];
            if (offset < 0) {
                offset += (1<<30);
                if ((arena->cells[offset] & CELLMASK) != 0) {
                    buffer[i] = 'o';
                } else {
                    buffer[i] = 'b';
                };
            } else {
                if ((arena->cells[offset] & NULLMASK) != 0) {
                    buffer[i] = ' ';
                } else if ((arena->cells[offset] & CELLMASK) != 0) {
                    buffer[i] = 'O';
                } else {
                    buffer[i] = 'B';
                };
            };
        };
    };
    buffer[i] = 0;
    return buffer;
};




long successcount = 0;
long failurecount = 0;

int finallookahead(struct arena *arena,
                   int *minlookahead,
                   int *maxlookahead,
                   unsigned char *cells)

{
    int *lookahead = minlookahead;

    int offset;

    while (lookahead <= maxlookahead) {
        offset = *lookahead;
        if (cellchange(arena, offset, CN0, ROLLBACKINCONSISTENT)) {
            lookahead++;
            continue;
        } else if (cellchange(arena, offset, CN1, ROLLBACKINCONSISTENT)) {
            lookahead++;
            continue;
        };

        /* if we fall through here,
         * this branch of the search tree was not successful.
         * was not successful.
         */

        while (1) {
            if (lookahead == minlookahead) {
                failurecount++;
                return 0;
            };
            lookahead--;
            offset = *lookahead;
            if ((cells[offset] & CELLMASK) != 0) {
                cellchange(arena, offset, CxN, ROLLBACKNEVER);
            } else {
                cellchange(arena, offset, CxN, ROLLBACKNEVER);
                if (cellchange(arena, offset, CN1, ROLLBACKINCONSISTENT)) {
                    lookahead++;
                    break; /* we're done popping the search tree */
                };
            };
        };
    };

    /* Success. Roll back the current state. */
    while (lookahead > minlookahead) {
        lookahead--;
        offset = *lookahead;
        cellchange(arena, offset, CxN, ROLLBACKNEVER);
    };

    successcount++;
    return 1;
};

/*
 * rotation rules:
 *
 * Rotate on pop unless norotate is set
 * Set norotate if a node can generate an inconsistency.
 *
 */

int adaptivelookahead(struct arena *arena,
                      int *minlookahead,
                      int *maxlookahead,
                      unsigned char *cells)
{
    int *lookaheads = minlookahead;

    int offset0;
    int offset1;

    unsigned int norotate;


    norotate = 0;

    offset0 = lookaheads[0];

    if (lookaheads == maxlookahead) {
        if (!cellchange(arena, offset0, CN0, ROLLBACKINCONSISTENT)) {
            if (!cellchange(arena, offset0, CN1, ROLLBACKINCONSISTENT)) {
                return 0;
            };
        };
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        return 1;
    };

    offset1 = lookaheads[1];
    lookaheads[0] = offset0 - (1<<30);
    if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
        lookaheads++;
        goto popnode_N;
    } else if (!cellchange(arena, offset1, CN0, ROLLBACKINCONSISTENT)) {
        if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
            lookaheads++;
            goto popnode_1;
        } else {
            offset0 = offset1;
            goto pushnode_1_only;
        };
    } else {
        offset0 = offset1;
        goto pushnode_0;
    };

pushnode_0_only:
    lookaheads++;
    norotate <<= 1;
    if (lookaheads >= maxlookahead) {
        goto success;
    };
    offset1 = lookaheads[1];
    assert(offset0 != offset1);
    assert(offset0 >= 0);
    assert(offset1 >= 0);

    /* 0
     *
     *
     */
    if (!cellchange(arena, offset1, CN0, ROLLBACKNEVER)) {
        /* 0
         * 0
         * x
         */
        cellchange(arena, offset1, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
            /* 000
             * 0 1
             * x x
             */
            if ((norotate & 2) == 0) {
                lookaheads[1] = offset0;
                lookaheads[0] = offset1;
                cellchange(arena, offset0, CxN, ROLLBACKNEVER);
                goto popnode_1;
            } else {
                cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                offset1 = offset0;
                goto popnode_0;
            };
        } else {
            /* 000
             * 0 1
             * x
             */
            norotate |= 1;
            offset0 = offset1;
            goto pushnode_1_only;
        };
    } else {
        /* 0
         * 0
         *
         */
        offset0 = offset1;
        goto pushnode_0;
    };

pushnode_1_only:
    lookaheads++;
    norotate <<= 1;
    if (lookaheads >= maxlookahead) {
        goto success;
    };
    offset1 = lookaheads[1];
    assert(offset0 != offset1);
    assert(offset0 >= 0);
    assert(offset1 >= 0);

    /* 1
     *
     *
     */
    if (!cellchange(arena, offset1, CN0, ROLLBACKINCONSISTENT)) {
        /* 11
         * 0
         * x
         */

        if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
            /* 111
             * 0 1
             * x x
             */
            if ((norotate & 2) == 0) {
                lookaheads[1] = offset0;
                lookaheads[0] = offset1;
                cellchange(arena, offset0, CxN, ROLLBACKNEVER);
                goto popnode_1;
            } else {
                cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                offset1 = offset0;
                goto popnode_1;
            };
        } else {
            /* 111
             * 0 1
             * x
             */
            offset0 = offset1;
            norotate |= 1;
            goto pushnode_1_only;
        };
    } else {
        /* 1
         * 0
         *
         */
        offset0 = offset1;
        goto pushnode_0;
    };

pushnode_0:
    lookaheads++;
    norotate <<= 1;
    if (lookaheads >= maxlookahead) {
        goto success;
    };
    offset1 = lookaheads[1];
    assert(offset0 != offset1);
    assert(offset0 >= 0);
    assert(offset1 >= 0);

    /* 0
     *
     *
     */
    if (!cellchange(arena, offset1, CN0, ROLLBACKNEVER)) {
        /* 00
         *  0
         *  x
         */
        if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
            /* 00
             *  00
             *  xx
             */
            cellchange(arena, offset1, CxN, ROLLBACKNEVER);
            if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 00
                 *  00 1
                 *  xx x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                goto popnode_1;
            } else if (!cellchange(arena, offset0, CN0, ROLLBACKINCONSISTENT)) {
                /* 00   0
                 *  00 111
                 *  xx  x
                 */
                if (!cellchange(arena, offset0, CN1, ROLLBACKINCONSISTENT)) {
                    /* 00   0 1
                     *  00 11111
                     *  xx  x x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else {
                    /* 00   0 1
                     *  00 1111
                     *  xx  x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate = (norotate & ~1) | 2;
                    goto pushnode_1_only;
                };
            } else {
                /* 00   0
                 *  00 11
                 *  xx
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate = (norotate & ~1) | 2;
                goto pushnode_0;
            };
        } else if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
            /* 00 1
             *  000
             *  x x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 00 11
                 *  000
                 *  x xx
                 */
                cellchange(arena, offset1, CN1, ROLLBACKNEVER);
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 00 111
                     *  000 11
                     *  x xxxx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
                    /* 00 111 0
                     *  000 111
                     *  x xxx x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    norotate |= 2;
                    offset1 = offset0;
                    goto popnode_0;
                } else {
                    /* 00 111 0
                     *  000 111
                     *  x xxx
                     */
                    norotate |= 3;
                    offset0 = offset1;
                    goto pushnode_1_only;
                };
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 00 111
                 *  000 1
                 *  x x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 00 111
                     *  000 11
                     *  x x xx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
                    /* 00 111 0
                     *  000 111
                     *  x x x x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_0;
                } else {
                    /* 00 111 0
                     *  000 111
                     *  x x x
                     */
                    norotate |= 1;
                    offset0 = offset1;
                    goto pushnode_1_only;
                };
            } else {
                /* 00 111
                 *  000 1
                 *  x x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 1;
                goto pushnode_1;
            };
        } else {
            /* 00 1
             *  000
             *  x
             */

            lookaheads[0] = offset1 - (1<<30);
            lookaheads[1] = offset0;
            norotate |= 1;
            goto pushnode_1_only;
        };
    } else {
        /* 00
         *  0
         *
         */
        lookaheads[0] = offset0 - (1<<30);
        offset0 = offset1;
        goto pushnode_0;
    };

pushnode_1:
    lookaheads++;
    norotate <<= 1;
    if (lookaheads >= maxlookahead) {
        goto success;
    };
    offset1 = lookaheads[1];
    assert(offset0 != offset1);
    assert(offset0 >= 0);
    assert(offset1 >= 0);

    /* 1
     *
     *
     */
    if (!cellchange(arena, offset1, CN0, ROLLBACKNEVER)) {
        /* 11
         *  0
         *  x
         */
        if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
            /* 11
             *  00
             *  xx
             */
            cellchange(arena, offset1, CxN, ROLLBACKNEVER);
            if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 11
                 *  00 1
                 *  xx x
                 */
                lookaheads[1] = offset0;
                lookaheads[0] = offset1;
                norotate |= 2;
                goto popnode_1;
            } else if (!cellchange(arena, offset0, CN1, ROLLBACKINCONSISTENT)) {
                /* 11   1
                 *  00 111
                 *  xx  x
                 */
                if (!cellchange(arena, offset0, CN0, ROLLBACKINCONSISTENT)) {
                    /* 11   1 0
                     *  00 11111
                     *  xx  x x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else {
                    /* 11   1 0
                     *  00 1111
                     *  xx  x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 3;
                    goto pushnode_0_only;
                };
            } else {
                /* 11   1
                 *  00 11
                 *  xx
                 */
                lookaheads[1] = offset0;
                lookaheads[0] = offset1;
                norotate |= 2;
                goto pushnode_1;
            };
        } else if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
            /* 11 0
             *  000
             *  x x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 11 00
                 *  000
                 *  x xx
                 */
                cellchange(arena, offset1, CN1, ROLLBACKNEVER);
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 11 000
                     *  000 11
                     *  x xxxx
                     */
                    lookaheads[1] = offset0;
                    lookaheads[0] = offset1;
                    norotate |= 2;
                    goto popnode_1;
                } else if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
                    /* 11 000 1
                     *  000 111
                     *  x xxx x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    norotate |= 2;
                    offset1 = offset0;
                    goto popnode_1;
                } else {
                    /* 11 000 1
                     *  000 111
                     *  x xxx
                     */
                    norotate |= 3;
                    offset0 = offset1;
                    goto pushnode_1_only;
                };
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 11 000
                 *  000 1
                 *  x x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 11 000
                     *  000 11
                     *  x x xx
                     */
                    lookaheads[1] = offset0;
                    lookaheads[0] = offset1;
                    norotate |= 2;
                    goto popnode_1;
                } else if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
                    /* 11 000 1
                     *  000 111
                     *  x x x x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_1;
                } else {
                    /* 11 000 1
                     *  000 111
                     *  x x x
                     */
                    norotate |= 1;
                    offset0 = offset1;
                    goto pushnode_1_only;
                };
            } else {
                /* 11 000
                 *  000 1
                 *  x x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 1;
                goto pushnode_0;
            };
        } else {
            /* 11 0
             *  000
             *  x
             */
            lookaheads[0] = offset1 - (1<<30);
            lookaheads[1] = offset0;
            norotate |= 1;
            goto pushnode_0_only;
        };
    } else {
        /* 11
         *  0
         *
         */
        lookaheads[0] = offset0 - (1<<30);
        offset0 = offset1;
        goto pushnode_0;
    };

popnode_0or1:
    if (lookaheads <= minlookahead) {
        goto failure;
    };
    lookaheads--;
    norotate >>= 1;
    offset0 = lookaheads[0];
    assert(offset0 != offset1);
    assert(offset1 >= 0);

    if (offset0 >= 0) {
        /* Node is finished.*/
        if ((norotate & 2) == 0) {
            lookaheads[0] = offset1;
            lookaheads[1] = offset0;
            cellchange(arena, offset0, CxN, ROLLBACKNEVER);
            goto popnode_0or1;
        } else {
            cellchange(arena, offset1, CxN, ROLLBACKNEVER);
            offset1 = offset0;
            goto popnode_0or1;
        };
    } else if ((cells[offset1] & CELLMASK) != 0) {
        goto popnode_1_check;
    } else {
        goto popnode_0_check;
    };

popnode_N:
    if (lookaheads <= minlookahead) {
        goto failure;
    };
    lookaheads--;
    norotate >>= 1;
    offset0 = lookaheads[0];
    assert(offset0 != offset1);
    assert(offset1 >= 0);

    if (offset0 >= 0) {
        /* Node is finished.*/
        if ((norotate & 2) == 0) {
            cellchange(arena, offset0, CxN, ROLLBACKNEVER);
            lookaheads[0] = offset1;
            lookaheads[1] = offset0;
            goto popnode_N;
        } else {
            offset1 = offset0;
            goto popnode_0or1;
        };
    } else {
        goto popnode_N_check;
    };

popnode_N_check:
    offset0 += (1<<30);
    lookaheads[0] = offset0;
    if ((cells[offset0] & CELLMASK) != 0) {
        /* 1
         *
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset1, CN0, ROLLBACKINCONSISTENT)) {
            /* 1
             *   0
             *   x
             */
            if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 1
                 *   0 1
                 *   x x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                goto popnode_1;
            } else if (!cellchange(arena, offset0, CN0, ROLLBACKINCONSISTENT)) {
                /* 1    0
                 *   0 111
                 *   x  x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                goto popnode_1;
            } else {
                /* 1    0
                 *   0 11
                 *   x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                norotate &= ~1;
                goto pushnode_0_only;
            };
        } else if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
            /* 1  0
             *   00
             *    x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 1  00
                 *   00
                 *    xx
                 */
                norotate |= 2;
                offset1 = offset0;
                goto popnode_0;
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 1  000
                 *   00 1
                 *    x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 1  000
                     *   00 11
                     *    x xx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else {
                    /* 1  000
                     *   00 11
                     *    x x
                     */
                    if ((norotate & 2) == 0) {
                        lookaheads[0] = offset1;
                        lookaheads[1] = offset0;
                        goto popnode_1;
                    } else {
                        cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                        offset1 = offset0;
                        goto popnode_N;
                    };
                };
            } else {
                /* 1  000
                 *   00 1
                 *    x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_1_only;
            };
        } else {
            /* 1  0
             *   00
             *
             */
           offset0 = offset1;
           goto pushnode_0;
        };
    } else {
        /* 0
         *
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset1, CN0, ROLLBACKINCONSISTENT)) {
            /* 0
             *   0
             *   x
             */
            if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 1
                 *   0 1
                 *   x x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                goto popnode_1;
            } else if (!cellchange(arena, offset0, CN1, ROLLBACKINCONSISTENT)) {
                /* 0    1
                 *   0 111
                 *   x  x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                goto popnode_1;
            } else {
                /* 0    1
                 *   0 11
                 *   x
                 */
                lookaheads[0] = offset1;
                lookaheads[1] = offset0;
                norotate |= 2;
                norotate &= ~1;
                goto pushnode_1_only;
            };
        } else if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
            /* 0  1
             *   00
             *    x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 0  11
                 *   00
                 *    xx
                 */
                norotate |= 2;
                offset1 = offset0;
                goto popnode_1;
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 0  111
                 *   00 1
                 *    x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 0  111
                     *   00 11
                     *    x xx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else {
                    /* 0  111
                     *   00 11
                     *    x x
                     */
                    if ((norotate & 2) == 0) {
                        lookaheads[0] = offset1;
                        lookaheads[1] = offset0;
                        goto popnode_1;
                    } else {
                        cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                        offset1 = offset0;
                        goto popnode_N;
                    };
                };
            } else {
                /* 0  111
                 *   00 1
                 *    x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_1_only;
            };
        } else {
            /* 0  1
             *   00
             *
             */
           offset0 = offset1;
           goto pushnode_0;
        };
    };

popnode_1:
    if (lookaheads <= minlookahead) {
        goto failure;
    };
    lookaheads--;
    norotate >>= 1;
    offset0 = lookaheads[0];
    assert(offset0 != offset1);
    assert(offset1 >= 0);

    if (offset0 >= 0) {
        /* Node is finished.*/
        if ((norotate & 2) == 0) {
            lookaheads[0] = offset1;
            lookaheads[1] = offset0;
            cellchange(arena, offset0, CxN, ROLLBACKNEVER);
            goto popnode_1;
        } else {
            cellchange(arena, offset1, CxN, ROLLBACKNEVER);
            offset1 = offset0;
            goto popnode_0or1;
        };
    } else {
        goto popnode_1_check;
    };

popnode_1_check:
    offset0 += (1<<30);
    lookaheads[0] = offset0;
    if ((cells[offset0] & CELLMASK) != 0) {
        /* 1
         * 1
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
            /* 1 0
             * 111
             *   x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 1 00
                 * 111
                 *   xx
                 */
                offset1 = offset0;
                norotate |= 2;
                goto popnode_0;
            } else if (!cellchange(arena, offset1, CN0, ROLLBACKNEVER)) {
                /* 1 000
                 * 111 0
                 *   x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 1 000
                     * 111 00
                     *   x xx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_0;
                } else if ((norotate & 2) != 0) {
                    /* 1 000
                     * 111 00
                     *   x x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_N;
                } else {
                    /* 1 000
                     * 111 00
                     *   x x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    goto popnode_0;
                };
            } else {
                /* 1 000
                 * 111 0
                 *   x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_0_only;
            };
        } else {
            /* 1 0
             * 111
             *
             */
            offset0 = offset1;
            goto pushnode_1;
        };
    } else {
        /* 0
         * 1
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
            /* 0 1
             * 111
             *   x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 0 11
                 * 111
                 *   xx
                 */
                norotate |= 2;
                offset1 = offset0;
                goto popnode_1;
            } else if (!cellchange(arena, offset1, CN0, ROLLBACKNEVER)) {
                /* 0 111
                 * 111 0
                 *   x x
                 */
                if ((norotate & 2) == 0) {
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    cellchange(arena, offset0, CxN, ROLLBACKNEVER);
                    goto popnode_0;
                } else {
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_1;
                };
            } else {
                /* 0 111
                 * 111 0
                 *   x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_0_only;
            };
        } else {
            /* 0 1
             * 111
             *
             */
            offset0 = offset1;
            goto pushnode_1;
        };
    };

popnode_0:
    if (lookaheads <= minlookahead) {
        goto failure;
    };
    lookaheads--;
    norotate >>= 1;
    offset0 = lookaheads[0];
    assert(offset0 != offset1);
    assert(offset1 >= 0);

    if (offset0 >= 0) {
        /* Node is finished.*/
        if ((norotate & 2) == 0) {
            lookaheads[0] = offset1;
            lookaheads[1] = offset0;
            cellchange(arena, offset0, CxN, ROLLBACKNEVER);
            goto popnode_0;
        } else {
            cellchange(arena, offset1, CxN, ROLLBACKNEVER);
            offset1 = offset0;
            goto popnode_0or1;
        };
    } else {
        goto popnode_0_check;
    };

popnode_0_check:
    offset0 += (1<<30);
    lookaheads[0] = offset0;
    if ((cells[offset0] & CELLMASK) != 0) {
        /* 1
         * 0
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset0, CN0, ROLLBACKNEVER)) {
            /* 1 0
             * 000
             *   x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 1 00
                 * 000
                 *   xx
                 */
                norotate |= 2;
                offset1 = offset0;
                goto popnode_0;
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 1 000
                 * 000 1
                 *   x x
                 */
                if (!cellchange(arena, offset0, CxN, ROLLBACKNEVER)) {
                    /* 1 000
                     * 000 11
                     *   x xx
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    norotate |= 2;
                    goto popnode_1;
                } else if ((norotate & 2) != 0) {
                    /* 1 000
                     * 000 11
                     *   x x
                     */
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_N;
                } else {
                    /* 1 000
                     * 000 11
                     *   x x
                     */
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    goto popnode_1;
                };
            } else {
                /* 1 000
                 * 000 1
                 *   x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_1_only;
            };
        } else {
            /* 1 0
             * 000
             *
             */
            offset0 = offset1;
            goto pushnode_0;
        };
    } else {
        /* 0
         * 0
         *
         */
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
        if (!cellchange(arena, offset0, CN1, ROLLBACKNEVER)) {
            /* 0 1
             * 000
             *   x
             */
            if (!cellchange(arena, offset1, CxN, ROLLBACKNEVER)) {
                /* 0 11
                 * 000
                 *   xx
                 */
                norotate |= 2;
                offset1 = offset0;
                goto popnode_1;
            } else if (!cellchange(arena, offset1, CN1, ROLLBACKNEVER)) {
                /* 0 111
                 * 000 1
                 *   x x
                 */
                if ((norotate & 2) == 0) {
                    lookaheads[0] = offset1;
                    lookaheads[1] = offset0;
                    cellchange(arena, offset0, CxN, ROLLBACKNEVER);
                    goto popnode_1;
                } else {
                    cellchange(arena, offset1, CxN, ROLLBACKNEVER);
                    offset1 = offset0;
                    goto popnode_1;
                };
            } else {
                /* 0 111
                 * 000 1
                 *   x
                 */
                norotate |= 1;
                offset0 = offset1;
                goto pushnode_1_only;
            };
        } else {
            /* 0 1
             * 000
             *
             */
            offset0 = offset1;
            goto pushnode_0;
        };
    };



success:
    successcount++;
    offset0 = lookaheads[0];
    if (offset0 < 0) {
        offset0 += (1<<30);
    };
    lookaheads[0] = offset0;

    if ((cells[offset0] & NULLMASK ) == 0) {
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
    };
    while (lookaheads > minlookahead) {
        lookaheads--;
        offset0 = lookaheads[0];
        if (offset0 < 0) {
            offset0 += (1<<30);
            lookaheads[0] = offset0;
        };
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
    };
    return 1;

failure:
    failurecount++;
    offset0 = lookaheads[0];
    if ((cells[offset0] & NULLMASK) == 0) {
        cellchange(arena, offset0, CxN, ROLLBACKNEVER);
    };
    return 0;

};

void clearcache(struct searchnode *node)
{
    if (node->cachecount > 27) {
        memset(node->cache3, 0, sizeof(uint64_t) << (node->cachecount - 27));
    } else if (node->cachecount > 0) {
        node->cache3[0] = 0;
    };

};

uint64_t masks[7] = {0x1ULL,
                     0x3ULL,
                     0xFULL,
                     0xFFULL,
                     0xFFFFULL,
                     0xFFFFFFFFULL,
                     0xFFFFFFFFFFFFFFFFULL};


int testcache0(struct searchnode *node,
               unsigned int addr,
               char index)
{
    uint64_t v;

    if (index >= 28 || index < 0 || !node->cachepopulated) {
        return 1;
    }

    if (index >= 21) {
        v = node->cache3[addr >> 27] >> ((addr >> 21) & 63);
        return (v & masks[index - 21]) != 0;
    };
    if ((node->cache3[addr >> 27] &
         (1UL << ((addr >> 21) & 63))) == 0) {
        return 0;
    };
    if (index >= 14) {
        v = node->cache2[addr >> 20] >> ((addr >> 14) & 63);
        return (v & masks[index - 14]) != 0;
    };
    if ((node->cache2[addr >> 20] &
         (1UL << ((addr >> 14) & 63))) == 0) {
        return 0;
    };
    if (index >= 7) {
        v = node->cache1[addr >> 13] >> ((addr >> 7) & 63);
        return (v & masks[index - 7]) != 0;
    };
    if ((node->cache1[addr >> 13] &
         (1UL << ((addr >> 7) & 63))) == 0) {
        return 0;
    };
    v = node->cache0[addr >> 6] >> (addr & 63);
    return (v & masks[index]) != 0;
}

int testcache1(struct searchnode *node,
               unsigned int addr,
               char index)
{
    uint64_t v;
    if (index >= 28 || index < 0 || !node->cachepopulated) {
        return 1;
    };

    addr += (1ULL << index);

    if (index >= 21) {
        v = node->cache3[addr >> 27] >> ((addr >> 21) & 63);
        return (v & masks[index - 21]) != 0;
    };
    if ((node->cache3[addr >> 27] &
         (1UL << ((addr >> 21) & 63))) == 0) {
        return 0;
    };
    if (index >= 14) {
        v = node->cache2[addr >> 20] >> ((addr >> 14) & 63);
        return (v & masks[index - 14]) != 0;
    };
    if ((node->cache2[addr >> 20] &
         (1ULL << ((addr >> 14) & 63))) == 0) {
        return 0;
    };
    if (index >= 7) {
        v = node->cache1[addr >> 13] >> ((addr >> 7) & 63);
        return (v & masks[index - 7]) != 0;
    };
    if ((node->cache1[addr >> 13] &
         (1ULL << ((addr >> 7) & 63))) == 0) {
        return 0;
    };
    v = node->cache0[addr >> 6] >> (addr & 63);
    return (v & masks[index]) != 0;
}

void setcache(struct searchnode *node,
             unsigned int addr) {
    uint64_t v;

    v = node->cache3[addr >> 27];
    if ((v & (1ULL << ((addr >> 21) & 63))) == 0) {
        node->cache2[addr >> 20] = 0;
        node->cache2[(addr >> 20) ^ 1] = 0;
        node->cache3[addr >> 27] = v + (1ULL << ((addr >> 21) & 63));
    };
    v = node->cache2[addr >> 20];
    if ((v & (1ULL << ((addr >> 14) & 63))) == 0) {
        node->cache1[addr >> 13] = 0;
        node->cache1[(addr >> 13) ^ 1] = 0;
        node->cache2[addr >> 20] = v + (1ULL << ((addr >> 14) & 63));
    };
    v = node->cache1[addr >> 13];
    if ((v & (1ULL << ((addr >> 7) & 63))) == 0) {
        node->cache0[addr >> 6] = 0;
        node->cache0[(addr >> 6) ^ 1] = 0;
        node->cache1[addr >> 13] = v + (1ULL << ((addr >> 7) & 63));
    };
    node->cache0[addr >> 6] |= (1ULL << (addr & 63));
};

long int lookaheadhist_succ[16];
long int lookaheadhist_fail[16];
long int lookaheadhist_mismatch[16];


int lookahead(struct searchnode *node, struct arena *arena,
              unsigned char *cells)
{
    int *lookaheads = node->lookaheadoffsets;
    int *maxlookahead = lookaheads + node->lookaheadcount - 1;
    int i;
    int threshold = node->threshold;
    int offset;
    int rv;
    unsigned int addr[3];
    int cachei;
    struct searchnode *cachenode;
    int looki;
    int arenai;
    unsigned long lookstate;

    lookstate = 0;

    for (cachei = 0; cachei < 3; cachei++) {
        addr[cachei] = 0;
        if (cachei == 0) {
            cachenode = node;
        } else {
            cachenode = node->cachenodes[cachei-1];
            if (!cachenode) {
                continue;
            };
        };

        for (i = 0; i < cachenode->cachecount; i++) {
            addr[cachei] <<= 1;
            if (cells[cachenode->cacheoffsets[i]] & CELLMASK) {
                addr[cachei] += 1;
            };
        };
    };

    if (!node->cachepopulated) {
        node->cachepopulated = 1;
        clearcache(node);
    };

    looki = 0;
    arenai = 0;
    rv = 0;
    lookstate = 0;

    while (looki > -1) {

        if (terminate) {
            node->cachepopulated = 0;
            rv = 1;
            break;
        };


        /* try to advance looki */
        while (looki < threshold) {
            if ((node->cachebits[0][looki] != 0 ||
                !testcache0(node, addr[0], node->cachebits[0][looki])) &&
                testcache0(node->cachenodes[0], addr[1],
                           node->cachebits[1][looki]) &&
                testcache0(node->cachenodes[1], addr[2],
                           node->cachebits[2][looki])) {
                looki++;
            } else {
                break;
            };
        };


        /* See if we can try to complete the lookahead */
        if (looki >= threshold) {
            for (i = arenai; i < looki; i++) {
                offset = lookaheads[i];
                if ((cells[offset] & NULLMASK) == 0) {
                    if (((cells[offset] & CELLMASK ) != 0)
                        !=
                        ((lookstate & (1UL << i)) != 0)) {
                        cellchange(arena, offset, CxN, ROLLBACKNEVER);
                    };
                };
            };

            while (arenai < threshold) {
                offset = lookaheads[arenai];
                if ((cells[offset] & NULLMASK) != 0) {
                    if (!cellchange(arena, offset,
                                    (((lookstate & (1UL << arenai)) == 0)
                                     ? CN0 : CN1),
                                    ROLLBACKINCONSISTENT)) {
                        if (threshold - arenai < 16) {
                            lookaheadhist_fail[threshold - arenai]++;
                        };
                        break;
                    };
                    if (threshold - arenai < 16) {
                        lookaheadhist_succ[threshold - arenai]++;
                    };
                };
                arenai++;
            };

            if (arenai >= threshold) {
                if (arenai >= node->lookaheadcount ||
                    adaptivelookahead(arena,
                                      &lookaheads[arenai],
                                      maxlookahead,
                                      cells)) {
                    if (node->cachecount > 0) {
                        setcache(node, addr[0]);
                    };
                    rv = 1;

                    /* roll back looki to the point where
                     * we will advance to the next cache address, as there
                     * is no point doing anything further with the current
                     * cache address. */
                    looki--;
                    while(looki > -1 && node->cachebits[0][looki] != 0 ) {
                        if ((lookstate & (1UL << looki)) != 0) {
                            lookstate -= 1UL << looki;
                            if (node->cachebits[0][looki] >= 0) {
                                addr[0] -= 1 << node->cachebits[0][looki];
                            };
                            if (node->cachebits[1][looki] >= 0) {
                                addr[1] -= 1 << node->cachebits[1][looki];
                            };
                            if (node->cachebits[2][looki] >= 0) {
                                addr[2] -= 1 << node->cachebits[2][looki];
                            };
                        };
                        looki--;
                    };
                    arenai = looki;
                } else {
                    arenai--;
                    looki--;
                };
            } else {
                looki--;
            };

            /* Roll back past any null cells at the end as those states have
             * already been ruled out.
             */

            while (looki > arenai && (cells[lookaheads[looki]] & NULLMASK) != 0) {
                if ((lookstate & (1UL << looki)) != 0) {
                    lookstate -= 1UL << looki;
                    if (node->cachebits[0][looki] >= 0) {
                        addr[0] -= 1 << node->cachebits[0][looki];
                    };
                    if (node->cachebits[1][looki] >= 0) {
                        addr[1] -= 1 << node->cachebits[1][looki];
                    };
                    if (node->cachebits[2][looki] >= 0) {
                        addr[2] -= 1 << node->cachebits[2][looki];
                    };
                };
                looki--;
            };

            if (arenai > looki) {
                arenai = looki;
            };

        };

        /* now advance the state */

        while (looki > -1) {
            if ((lookstate & (1UL << looki)) == 0) {
                if ((node->cachebits[0][looki] != 0 ||
                    !testcache1(node, addr[0], node->cachebits[0][looki])) &&
                    testcache1(node->cachenodes[0], addr[1],
                               node->cachebits[1][looki]) &&
                    testcache1(node->cachenodes[1], addr[2],
                               node->cachebits[2][looki])) {
                    lookstate += 1UL << looki;
                    if (node->cachebits[0][looki] >= 0) {
                        addr[0] += 1 << node->cachebits[0][looki];
                    };
                    if (node->cachebits[1][looki] >= 0) {
                        addr[1] += 1 << node->cachebits[1][looki];
                    };
                    if (node->cachebits[2][looki] >= 0) {
                        addr[2] += 1 << node->cachebits[2][looki];
                    };
                    looki++;
                    break; /* advanced */
                }  else {
                    looki--;
                    if (arenai > looki) {
                        arenai = looki;
                    };
                };
            } else {
                lookstate -= 1UL << looki;
                if (node->cachebits[0][looki] >= 0) {
                    addr[0] -= 1 << node->cachebits[0][looki];
                };
                if (node->cachebits[1][looki] >= 0) {
                     addr[1] -= 1 << node->cachebits[1][looki];
                };
                if (node->cachebits[2][looki] >= 0) {
                    addr[2] -= 1 << node->cachebits[2][looki];
                };
                looki--;
                if (arenai > looki) {
                    arenai = looki;
                };
            };
        };
    };

    for (i = 0; i < threshold; i++) {
        offset = lookaheads[i];
        if ((cells[offset] & NULLMASK) == 0) {
            cellchange(arena, offset, CxN, ROLLBACKNEVER);
        };
    };

    return rv;
};


void printoccupier(struct searchconfig *config)
{
    struct searchnode *n;
    char str[81];
    int i;
    unsigned char cell;

    n = config->tail;

    for (i = 0; i < 80; i++) {
        if (!n) {
            break;
        };
        cell = config->arena.cells[n->offset];
        if (cell & NULLMASK) {
            break;
        } else if (cell & CELLMASK) {
            str[i] = '1';
        } else {
            str[i] = '0';
        };
        n = n->prev;
    };

    str[i] = '\n';
    str[i+1] = 0;

    fputs(str,stderr);
};



void search(struct searchconfig *config, FILE *out)
{

    unsigned char *cells = config->arena.cells;
    unsigned char cell;
    int arenax = config->arena.arenax;
    struct arena *arena = &config->arena;

    struct searchnode *node;

    int offset;
    int discrepancy;

    int itercount = config->itercount;

    int occupierinterval = config->occupierinterval;

    node = config->head;

    while (node) {

        if (itercount > 0) {
            if (--itercount == 0) {
                terminate = 1;
            };
        };


        offset = node->offset;
        cell = cells[offset];
        if (cell & NULLMASK) {
            if (cellchange(arena, offset, CN0, ROLLBACKNEVER) &&
                (node->lookaheadcount == 0 ||
                 lookahead(node, arena, cells))) {
                node = node->prev;
                if (!node) {
                    /* we've found a solution. */

                    /* Roll back node type 'N', it's not considered
                     * part of the partial solution
                     */

                    node = config->head;

                    while (node && node->type == 'N') {
                        clearcache(node);
                        cellchange(arena, node->offset, CxN, ROLLBACKNEVER);
                        node = node->next;
                    };

                    print_ru_utime("solution", config, out);
                    printrle(arena, out);
                    fflush(out);
                    fputs("Solution Found!\n", stderr);
                };
            };
            if (occupierinterval > 0) {
                if (--occupierinterval == 0) {
                    printoccupier(config);
                    occupierinterval = config->occupierinterval;
                };
            };

        } else if (terminate || (itercount > 0 && --itercount == 0)) {
            /* this is a safe place to terminate because
             * if this is a solution it has already been printed out.
             */
            print_stats("stats", out);

            print_ru_utime("startrle", config, out);
            printrle(arena, out);
            fflush(out);

            if ((int)terminate == SIGUSR1) {
                terminate = 0;
            } else {
                break;
            };
        } else if (cell & CELLMASK) {
            clearcache(node);
            cellchange(arena, offset, CxN, ROLLBACKNEVER);
            node = node->next;
        } else {
            clearcache(node);
            cellchange(arena, offset, CxN, ROLLBACKNEVER);
            if (cellchange(arena, offset, CN1, ROLLBACKNEVER) &&
                (node->lookaheadcount == 0 ||
                 lookahead(node, arena, cells))) {
                node = node->prev;
                if (!node) {
                    /* we've found a solution. */

                    /* Roll back node type 'N', it's not considered
                     * part of the partial solution
                     */

                    node = config->head;

                    while (node && node->type == 'N') {
                        clearcache(node);
                        cellchange(arena, node->offset, CxN, ROLLBACKNEVER);
                        node = node->next;
                    };


                    print_ru_utime("solution", config, out);
                    printrle(arena, out);
                    fflush(out);
                    fputs("Solution Found!\n", stderr);
                };
                if (occupierinterval > 0) {
                    if (--occupierinterval == 0) {
                        printoccupier(config);
                        occupierinterval = config->occupierinterval;
                    };
            };
            } else {
                if (occupierinterval > 0) {
                    if (--occupierinterval == 0) {
                        printoccupier(config);
                        occupierinterval = config->occupierinterval;
                    };
                };

                cellchange(arena, offset, CxN, ROLLBACKNEVER);
                node = node->next;
            };

        };
    };

};

void printcachestats(struct searchconfig *config, FILE *out)
{
    struct searchnode *node = config->head;
    unsigned int i;
    long count;
    long othercount;
    long llccount;
    char state;

    fputs("stats cache s    cache set   cache size %set %llcset %neighbors\n",
          out);

    while (node) {
        if (node->cachecount > 0 &&
            (config->arena.cells[node->offset] & NULLMASK) == 0) {

            if ((config->arena.cells[node->offset] & CELLMASK) == 0) {
                state = 'b';
            } else {
                state = 'o';
            };

            if (node->cachepopulated) {
                count = 0;
                othercount = 0;
                for (i = 0; i < 1 << node->cachecount; i++) {
                    if (testcache0(node, i, 0)) {
                        count++;
                        if (testcache0(node, i ^ 1, 0)) {
                            othercount++;
                        };
                    };
                };

                llccount = 0;
                for (i = 0; i < 1 << node->cachecount; i += (1<<14)) {
                    if (testcache0(node, i, 14)) {
                        llccount++;
                    };
                };

                fprintf(out,
                        "stats cache %c %12ld %12d %3ld%% %3ld%% %3ld%%\n",
                        state,
                        count,
                        1<<node->cachecount,
                        count * 100 / (1<<node->cachecount),
                        llccount * 100 / (node->cachecount > 14 ?
                                                1 << (node->cachecount - 14) :
                                                1),
                        count > 0 ? othercount * 100 / count : 0);
            } else {
                fprintf(out,
                        "stats cache %c            * %12d   *    *    *\n",
                        state,
                        1<<node->cachecount);
            };
        };
        node = node->next;
    };
};

void cleanupconfig(struct searchconfig *config)
{
    struct searchnode *node;
    struct searchnode *nextnode;

    node = config->head;

    while (node) {
        nextnode = node->next;
        if (node->lookaheadcount > 0) {
            free(node->lookaheadoffsets);
            free(node->cachebits[0]);
        };
        if (node->cachecount > 0) {
            free(node->cacheoffsets);
            free(node->cache3);
        };
        free(node);
        node = nextnode;
    };

    cleanuparena(&config->arena);
};

void initlookaheadhist()
{
    int i;

    for (i = 0; i < 16; i++) {
        lookaheadhist_succ[i] = 0;
        lookaheadhist_fail[i] = 0;
        lookaheadhist_mismatch[i] = 0;
    };
};

void printlookaheadhist(FILE *out)
{
    int i;

    fputs("stats lookahead histogram\n", out);

    for (i = 1; i < 16; i++) {
        fprintf(out, "stats %3ld%% %12ld %12ld %12ld\n",
                lookaheadhist_succ[i] > 0 ?
                100 * lookaheadhist_succ[i] /
                (lookaheadhist_fail[i] + lookaheadhist_succ[i]) : 0,
                lookaheadhist_succ[i],
                lookaheadhist_succ[i] + lookaheadhist_fail[i],
                lookaheadhist_mismatch[i]);
    };
};


int main (int argc, char *argv[])
{
    struct searchconfig config;

    int i;

    if ( SIG_ERR == signal(SIGTERM, sigtermhandler) ||
         SIG_ERR == signal(SIGINT, sigtermhandler) ||
         SIG_ERR == signal(SIGUSR1, sigtermhandler)) {
        fputs("unable to register signal handler.\n", stderr);
        abort();
    };


    processconfig(&config, stdin, stdout);

    config.itercount = 0;
    for (i = 1; i < argc; i++) {
        (void) sscanf(argv[i], "--itercount=%d", &config.itercount);
    };

    config.occupierinterval = 0;
    for (i = 1; i < argc; i++) {
        (void) sscanf(argv[i], "--occupierinterval=%d", &config.occupierinterval);
    };

    initlookaheadhist();

    search(&config, stdout);

    for (i = 1; i < argc; i++) {
        if (0 == strcmp(argv[i], "--cachestats")) {
            printcachestats(&config, stdout);
            printlookaheadhist(stdout);
        };
    };

    cleanupconfig(&config);
};
