
#include <stdint.h>


/*
 * Note types:
 * A - arena node - left Null
 * N - node past match area - if a partial solution is found these are all
 *     rolled back before printout and continuing.
 * m - match node - used for matching left and right halves of spaceship
 * n - normal node
 * l - lookahead node
 * c - cache node
 */

struct searchnode
{
    struct searchnode *prev;
    struct searchnode *next;
    char type;
    char cachepopulated;
    int offset;
    int cachecount;
    int *cacheoffsets;
    int lookaheadcount;
    int *lookaheadoffsets;
    uint64_t *cache0;
    uint64_t *cache1;
    uint64_t *cache2;
    uint64_t *cache3;
    signed char *cachebits[3];
    int threshold;
    struct searchnode *cachenodes[2];
    int x[3];
    int y[3];
};

struct searchconfig
{
    struct searchnode *head;
    struct searchnode *tail;

    struct arena arena;

    struct timeval ru_utime;
    long successcount;
    long failurecount;

    int itercount; /* Iteration count after which to exit.
                      Used for benchmarking. */

    int occupierinterval;
};

void processconfig(struct searchconfig *config, FILE *in, FILE *out);

extern long successcount;
extern long failurecount;
