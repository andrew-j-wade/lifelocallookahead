CFLAGS = -g -O2
#CFLAGS = -g -O2 -DNODEBUG

locallookahead:	locallookahead.o processconfig.o arena.o

locallookahead.o:	locallookahead.c arena.h locallookahead.h
processconfig.o:	processconfig.c arena.h locallookahead.h
arena.o:	arena.c arena.h
arenatest.o:	arenatest.c arena.h

arenatest:	arenatest.o arena.o

