
#include <stdint.h>

#define CELLMASK 1
#define NULLMASK 2
#define RECURRENCECELLMASK 4
#define RECURRENCENULLMASK 8

#define CELLCLEAR 0
#define CELLSET 1
#define CELLNULL 2

#define CN0 1
#define CN1 2
#define CxN 3
#define C1N 3
#define C0N 4

struct changeentry {
    unsigned char *cell;
    uint16_t transition;
};

/* transition uint16_ts contain two fields:
 * next state (1 byte) + change << 8 (1 byte)
 * Packing the two fields like this allows small efficiencies in cellchange
 */

struct arena {
    int x,y,period;
    int arenax, arenay;
    int genoffset, recurrenceoffset;
    int discrepancycount;
    unsigned int birthcounts;
    unsigned int survivalcounts;
    unsigned char statestatus[256];
    uint16_t transitiontable[8 * 256];
    struct changeentry *queue;
    unsigned char *cells;
};

void initarena(struct arena *arena);
void cleanuparena(struct arena *arena);

enum rollbackbehavior {ROLLBACKNEVER=0,
    ROLLBACKINCONSISTENT = 1,
    ROLLBACKALWAYS = 2};

int cellchange(struct arena *arena, int offset, int change,
               enum rollbackbehavior rollback);
/* returns true if no discrepancy */